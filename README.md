# Marvelsoft chat js
## To use
```bash
# Clone this repository
git clone https://gitlab.com/benderbinary/marvelsoft-chat-js
# Install dependencies
npm install
# Run the app and connect to localhost:8989
npm start
```