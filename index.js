class ChatApp {
    constructor() {
        this.loginForm = document.getElementById('loginForm');
        this.chatContainer = document.getElementById('chatContainer');
        this.wsURLInput = document.getElementById('wsURLInput');
        this.wsPortInput = document.getElementById('wsPortInput');
        this.form = document.getElementById('messageForm');

        this.loginForm.addEventListener('submit', (event) => {
            console.log('submit from loginForm')
            event.preventDefault();
            const wsURL = this.wsURLInput.value;
            const wsPort = this.wsPortInput.value;
            this.connectWebSocket(wsURL, wsPort);
        });
    }

    connectWebSocket(wsURL, wsPort) {
        this.ws = new WebSocket(`ws://${wsURL}:${wsPort}`);
        this.ws.binaryType = "blob";
        this.setupWebSocket();
        this.setupForm();
    }

    setupWebSocket() {
        this.ws.addEventListener("open", event => {
            console.log("WebSocket connection opened");
            this.loginForm.style.display = "none";
            this.chatContainer.style.display = "block";
        });

        this.ws.addEventListener("close", event => {
            console.log("WebSocket connection closed");
        });

        this.ws.onmessage = this.handleMessage.bind(this);
    }

    handleMessage(message) {
        const msgDiv = document.createElement('div');
        msgDiv.classList.add('msgCtn');
        msgDiv.innerHTML = message.data;
        document.getElementById('messages').appendChild(msgDiv);
    }

    setupForm() {
        this.form.addEventListener('submit', (event) => {
            event.preventDefault();
            const message = document.getElementById('inputBox').value;
            this.ws.send(message);
            console.log('sent message ', message);

            const msgDiv = document.createElement('div');
            msgDiv.classList.add('msgCtn');
            msgDiv.innerHTML = message;

            document.getElementById('messages').appendChild(msgDiv);
            document.getElementById('inputBox').value = '';
        });
    }
}

const chatApp = new ChatApp();
